import 'dart:html' hide Player;
import 'dart:convert';
import 'dart:math' as Math;
import 'package:game_loop/game_loop_html.dart';
import 'package:box2d/box2d.dart';
import 'package:webiero/message.dart';
import 'package:webiero/renderer_2d.dart';
import 'package:webiero/world.dart';
import 'package:polymer/polymer.dart';

@CustomTag("webiero-form")
class WFormElement extends PolymerElement {
  @observable
  var playerName = "dummy";

  @observable
  var serverAddress = getDefaultAddress();

  @observable
  var roomName = "default";

  WFormElement.created() : super.created();

  CanvasRenderingContext2D context;
  WebSocket socket = null;
  var connected = false;
  GameLoopHtml gameLoop = null;
  GameWorld world = null;
  Player player = null;
  var renderer;

  void connect() {
    print("$playerName connect to $serverAddress");

    if (connected) {
      disconnect();
    }

    socket = new WebSocket("ws://$serverAddress/ws");

    // Connect to the server.
    socket.onOpen.listen((e) {
      var message = new Message.ConnectPlayer(playerName);
      message.send(socket);
    });

    // Listen for messages from the server.
    socket.onMessage.listen(handleServerMessage);

    connected = true;
  }

  void disconnect() {
    if (!connected) {
      return;
    }

    print("Disconnecting.");

    connected = false;

    // Heds up for the server.
    new Message.DisconnectPlayer()
      ..send(socket);

    socket.close();
    if (renderer != null) {
      renderer.drawFinish();
    }
    if (gameLoop != null) {
      gameLoop.stop();
    }
  }

  void handleServerMessage(msg) {
    //print("Received from server: ${msg.data}");

    var message = Message.parse(msg.data);

    switch (message.type) {
      case kInitWorldState:
        handleInitWorldState(message);
        break;
      case kInitPlayerState:
        handleInitPlayerState(message);
        break;
      case kInitializationFinished:
        handleInitializationFinished();
        break;
      case kAddPlayer:
        handleAddPlayer(message);
        break;
      case kRemovePlayer:
        handleRemovePlayer(message);
        break;
      case kUpdatePlayerState:
        handleUpdatePlayerState(message);
        break;
      case kShoot:
        handleShoot(message);
        break;

      default:
        print("ERROR: Unknown message type: ${message.type}");
        break;
    }
  }

  void handleInitWorldState(message) {
    print('InitWorldState message received.');

    world = new GameWorld();
    world.fromJson(message.payload);
  }

  void handleInitPlayerState(message) {
    print('InitPlayerState message received.');

    assert(world != null);

    String playerId = message.payload;
    player = world.getPlayerWithId(playerId);

    print('Received player: $player');
  }

  void handleInitializationFinished() {
    print('InitializationFinished message received.');

    assert(world != null);
    assert(player != null);

    CanvasElement canvasElement = querySelector('#frontBuffer');

    renderer = new Renderer2D(canvasElement, world, player);
    gameLoop = new GameLoopHtml(canvasElement)
      ..updateTimeStep = GameWorld.TIME_STEP
      ..onUpdate = update
      ..onRender = renderer.render
      ..onResize = renderer.resize
      ..start();
  }

  void handleAddPlayer(message) {
    print('AddPlayer message received.');

    assert(world != null);

    Player newPlayer = world.createPlayerFromJson(message.payload);
    world.addPlayer(newPlayer);

    print('Added a new player: $newPlayer');
  }

  void handleRemovePlayer(message) {
    print('RemovePlayer message received.');

    assert(world != null);

    String playerId = message.payload;
    world.removePlayerWithId(playerId);

    print('Removed the new player with id: $playerId');
  }

  void handleUpdatePlayerState(message) {
    //print('UpdatePlayerState message received.');

    assert(world != null);

    String playerId = JSON.decode(message.payload)['id'];
    world.getPlayerWithId(playerId).fromJson(message.payload);
  }

  void handleShoot(message) {
    //print('Shoot message received.');

    assert(world != null);

    Bullet bullet = new Bullet(new Vector2(0.0, 0.0), new Vector2(0.0, 0.0), new Weapon.uzi(), world.box2DWorld);
    bullet.fromJson(message.payload);

    world.bullets.add(bullet);
  }

  var trackedPlayerNum = 0;
  var lastZoom = null;

  void update(GameLoopHtml gameLoop) {
    if (player.energy > 0) {
      handleKeyboard();
      player.linearVelocity.x *= (1 - 5 * gameLoop.dt);
    }
    world.update();
    var newExplosions = world.fetchExplosions();
    if (!newExplosions.isEmpty) {
      renderer.createExplosions(newExplosions);
    }

    // Send the updated state of this player to the server.
    new Message.UpdatePlayerState(player)
               ..send(socket);
  }

  void handleKeyboard() {
    if (gameLoop.keyboard.pressed(Keyboard.TILDE)) {
      renderer.currentPlayer =
          world.players[trackedPlayerNum++ % world.players.length];
    }
    if (gameLoop.keyboard.pressed(Keyboard.ENTER)) {
      player.jump(gameLoop.time);
    }
    if (gameLoop.keyboard.isDown(Keyboard.SPACE)) {
      player.shootStart(world, socket, gameLoop.time, gameLoop.dt);
    }
    if (gameLoop.keyboard.released(Keyboard.SPACE)) {
      player.shootFinish(world, socket, gameLoop.time);
    }
    if (gameLoop.keyboard.isDown(Keyboard.UP)) {
      player.shootingAngle += gameLoop.dt;
      player.shootingAngle = Math.min(player.shootingAngle, Math.PI/2);
    }
    if (gameLoop.keyboard.isDown(Keyboard.DOWN)) {
      player.shootingAngle -= gameLoop.dt;
      player.shootingAngle = Math.max(player.shootingAngle, -Math.PI/2);
    }
    if (gameLoop.keyboard.isDown(Keyboard.LEFT)) {
      player.moveLeft(gameLoop.dt);
    }
    if (gameLoop.keyboard.isDown(Keyboard.RIGHT)) {
      player.moveRight(gameLoop.dt);
    }
    if (gameLoop.keyboard.isDown(Keyboard.A)) {
      player.energy -= 1;
    }

    if (gameLoop.keyboard.isDown(Keyboard.Z)) {
      if (lastZoom == null || lastZoom + 0.01 < gameLoop.time) {
        renderer.incZoomLevel();
        lastZoom = gameLoop.time;
      }
    }
    if (gameLoop.keyboard.isDown(Keyboard.X)) {
      if (lastZoom == null || lastZoom + 0.01 < gameLoop.time) {
        renderer.decZoomLevel();
        lastZoom = gameLoop.time;
      }
    }
    for (int key = Keyboard.ONE; key <= Keyboard.NINE; key += 1) {
      if (gameLoop.keyboard.pressed(key)) {
        player.selectWeapon(key - Keyboard.ONE);
      }
    }
  }


}

String getDefaultAddress() {
  return window.location.host;
}


#!/usr/bin/env ruby

@dryrun = false
def exe(cmd, stop_on_fail = true)
  puts "Executing #{cmd}"
  return if @dryrun
  system(cmd) or exit(1)
end

exe("pub install")
exe("pub build")
for file in Dir['web/out/*.dart']
  exe("dart2js #{file} -o#{file}.js")
end
links_from_web = ["assets", "favicon.ico"]
for link in links_from_web do
  exe("ln -nfs #{File.expand_path("web/" + link)} web/out/#{link}")
end
exe("dart bin/server.dart #{ARGV.join(' ')}")



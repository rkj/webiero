import 'dart:io';
import 'dart:async';
import 'dart:math';
import 'package:box2d/box2d.dart';
import 'package:webiero/message.dart';
import 'package:webiero/world.dart';
import 'package:http_server/http_server.dart' show VirtualDirectory;

GameWorld world = null;

int numConnectedPlayers = 0;
int port = 5040;

// Map from player IDs to their socets.
Map sockets = new Map();

void addWebSocket(Player player, WebSocket socket) {
  sockets[player.id] = socket;
}

WebSocket getWebSocket(Player player) {
  return sockets[player.id];
}

void main(args) {
  const PORT = "--port";
  for (var arg in args) {
    if (arg.startsWith(PORT)) {
      port = int.parse(arg.substring(PORT.length+1));
    }
  }
  var portEnv = Platform.environment['PORT'];
  if (portEnv != null) {
    port = int.parse(portEnv);
  }
  world = createGameWorld();

  var script = new File(Platform.script.path);
  var directory = script.parent;
  var buildUri = Platform.script.resolve('../build/web/');

  var staticFiles = new VirtualDirectory(buildUri.toFilePath());
  staticFiles
      ..allowDirectoryListing = true
      ..errorPageHandler = (request) {
        var response = request.response;
        response.statusCode = HttpStatus.NOT_FOUND;
        response.writeln("Page not found!");
        response.close();
      }
      ..directoryHandler = (dir, request) {
        // Redirect directory-requests to piratebadge.html file.
        var indexUri = new Uri.file(dir.path).resolve('index.html');
        staticFiles.serveFile(new File(indexUri.toFilePath()), request);
      };
  HttpServer.bind('0.0.0.0', port)
    .then((HttpServer server) {
      print('Listening for connections on $port');

      var sc = new StreamController();
      sc.stream
        .transform(new WebSocketTransformer())
        .listen(handleSocket);

      server.listen((HttpRequest request) {
        if (request.uri.path == '/ws') {
          print("Adding web socket connection");
          sc.add(request);
          return;
        } else {
          print("serving file: " + request.uri.path);
          staticFiles.serveRequest(request);
        }
      });
    },
    onError: (error) => print("Error starting HTTP server: $error"));
}

GameWorld createGameWorld() {
  var w = new GameWorld();
  w.add_random_terrain(400.0, 10.0, 300.0);
  return w;
}

class ConnectionState {
  Player player;

}

void handleSocket(WebSocket ws) {
  print("Connection created $ws");

  ConnectionState state = new ConnectionState();

  var onData = (data) {
    //print("Received: $data");
    var message = Message.parse(data);
    //try {
      handleMessage(ws, state, message);
    //} catch (e) {
    //  print("Exception while handling message: $e");
    //}
  };

  var onError = (error) {
    print("Error from the socket $error");
  };

  ws.listen(onData, onError: onError, cancelOnError: true);
}

void handleMessage(WebSocket ws, ConnectionState state, Message message) {
  switch (message.type) {
    case kConnectPlayer:
      handleConnectPlayer(ws, state, message);
      break;
    case kDisconnectPlayer:
      handleDisconnectPlayer(ws, state, message);
      break;
    case kUpdatePlayerState:
      handleUpdatePlayerState(ws, state, message);
      break;
    case kShoot:
      handleShoot(ws, state, message);
      break;
    case kUpdateGameState:
      handleUpdateGameState(ws, state, message);
      break;

    default:
      print("ERROR: Unknown message type: ${message.type}");
      break;
  }
}

void broadcastMessage(Message message, var ignorePlayerId) {
  var brokenPlayers = [];
  world.players.where((player) => player.id != ignorePlayerId).forEach((player) {
    try {
      message.send(getWebSocket(player));
    } catch (error) {
      print("Player doesnt work: $player");
      brokenPlayers.add(player);
    }
  });
  for (var player in brokenPlayers) {
    removePlayer(player);
  }
}

void removePlayer(Player player) {
  var playerId = player.id;
  // Remove this player from the world.
  world.removePlayerWithId(playerId);
  sockets.remove(playerId);
  // Inform other players that this player is disconnecting.
  broadcastMessage(new Message.RemovePlayer(playerId), playerId);
}

void handleConnectPlayer(ws, state, message) {
  print('ConnectPlayer message received.');

  var rng = new Random();

  // Ugly :(
  num minY = 1000000000;
  for (Polygon p in world.terrain) {
    for (Vector2 v in p) {
      if (minY > v.y) {
        minY = v.y;
      }
    }
  }

  // Create a new player and add it to the world.
  Player newPlayer = world.createPlayer(
      message.payload,  // name
      // TODO: Generat the position and velocity in a smarter way.
      new Vector2(rng.nextDouble() * 2, minY - 20),
      new Vector2(rng.nextDouble(), rng.nextDouble()));

  newPlayer.id = (++numConnectedPlayers).toString();

  world.addPlayer(newPlayer);

  state.player = newPlayer;
  sockets[newPlayer.id] = ws;

  // Send the world to newly connected player.
  new Message.InitWorldState(world)..send(ws);

  // Initialize the new player state.
  new Message.InitPlayerState(newPlayer.id)..send(ws);

  // Finalize initialization.
  new Message.InitializationFinished()..send(ws);

  // Inform other players that a new player has joined.
  broadcastMessage(new Message.AddPlayer(newPlayer), newPlayer.id);
}

void handleDisconnectPlayer(ws, state, message) {
  print('DisconnectPlayer message received.');
  removePlayer(state.player);
}

void handleUpdatePlayerState(ws, state, message) {
  //print('UpdatePlayerState message received.');

  // Update the state of this player.
  state.player.fromJson(message.payload);

  // Broadcast to other players.
  broadcastMessage(new Message.UpdatePlayerState(state.player), state.player.id);
}

void handleShoot(ws, state, message) {
  //print('Shoot message received.');

  // Hack
  Bullet bullet = new Bullet(new Vector2(0.0, 0.0), new Vector2(0.0, 0.0), new Weapon.uzi(), world.box2DWorld);
  bullet.fromJson(message.payload);

  // Broadcast to other players.
  broadcastMessage(new Message.Shoot(bullet), state.player.id);
}

void handleUpdateGameState(ws, state, message) {
  print('UpdateGameState message received.');
}

part of world;

class Weapon extends ConvertibleToJson {
  num damage;
  num damageRadius;
  num shootingSpeed;
  num bulletMass;
  num radius;

  num _lastShotTime = 0.0;
  num reloadTime; // in seconds

  Weapon() {}

  Weapon.uzi() {
    // 600 rounds per minute
    reloadTime = 60 / 600;
    damageRadius = 1.5;
    damage = 5.0;
    shootingSpeed = 10.0;
    bulletMass = 0.01;
    radius = 0.1;
  }

  Weapon.bazooka() {
    reloadTime = 2.0;
    damageRadius = 5.0;
    damage = 75.0;
    shootingSpeed = 20.0;
    bulletMass = 2.0;
    radius = 0.5;
  }

  Map toMap() {
    return {
      'reloadTime': reloadTime,
      'damageRadius': damageRadius,
      'damage': damage,
      'shootingSpeed': shootingSpeed,
      'bulletMass': bulletMass,
      'radius': radius,
    };
  }

  void fromMap(Map map) {
    reloadTime = map['reloadTime'];
    damageRadius = map['damageRadius'];
    damage = map['damage'];
    shootingSpeed = map['shootingSpeed'];
    bulletMass = map['bulletMass'];
    radius = map['radius'];
  }

  void shoot(num time) {
    _lastShotTime = time;
  }

  bool isLoaded(num time) {
    return _lastShotTime + reloadTime < time;
  }
}


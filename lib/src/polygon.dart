part of world;

// Assuems Convex polygon
class Polygon  extends ConvertibleToJson {
  List<Vector2> vertices;

  Polygon() : vertices = new List<Vector2>() {}

  Iterator get iterator => vertices.iterator;

  // Serializatioin:

  Map toMap() {
    List xList = new List();
    List yList = new List();
    for (Vector2 v in vertices) {
      xList.add(v.x);
      yList.add(v.y);
    }
    return {
      'x': xList,
      'y': yList,
    };
  }

  void fromMap(Map map) {
    List x = map['x'];
    List y = map['y'];
    vertices = [];
    for (int i = 0; i < x.length; ++i) {
      vertices.add(new Vector2(x[i], y[i]));
    }
  }
}


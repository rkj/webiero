part of world;

class GameContact extends ContactListener {
  GameWorld gameWorld;

  GameContact(var world) {
    gameWorld = world;
  }

  void beginContact(Contact contact) {
    Fixture fixtureA = contact.fixtureA;
    Fixture fixtureB = contact.fixtureB;

    Body bodyA = fixtureA.body;
    Body bodyB = fixtureB.body;


    var worldA = bodyA.world;
    var worldB = bodyB.world;

   // print("Collision ");
    assert(worldA == worldB);

    handleContact(bodyA, bodyB, "Player", "Bullet", (Player first, Bullet bullet) {
     // print ("Player hit by Bullet");
     first.hitByBullet(bullet);
     gameWorld.createExplosion(bullet);
     bullet.destroy(gameWorld);
    });

    /** handleContact(bodyA, bodyB, "Player", "Polygon", (first, second){
      first.hitATerrain();
    });*/

    handleContact(bodyA, bodyB, "Bullet", "Polygon", (Bullet bullet, var polygon) {
      gameWorld.createExplosion(bullet);
      bullet.destroy(gameWorld);
    });


    handleContact(bodyA, bodyB, "Bullet", "Bullet", (first, second){
      //first.destroy(gameWorld);
      //second.destroy(gameWorld);
    });

    /**handleContact(bodyA, bodyB, "Player", "Player", (first, second) {
      first.hitAPlayer();
    });*/
  }

  void handleContact(bodyA, bodyB, expectedTypeA, expectedTypeB, method) {
    var typeA = bodyA.userData.runtimeType.toString();
    var typeB = bodyB.userData.runtimeType.toString();

    var dataA = bodyA.userData;
    var dataB = bodyB.userData;
    //print("inside handlecontact $typeA, $typeB> :: <$expectedTypeA>, <$expectedTypeB>");
    if (typeA == expectedTypeA && typeB == expectedTypeB) {
      method(dataA, dataB);
    } else if (typeA == expectedTypeB && typeB == expectedTypeA) {
      method(dataB, dataA);
    }
  }


  void endContact(Contact contact) {
    //print ("Contact ended");
  }

  void postSolve(Contact contact, ContactImpulse impulse) {
    //print ("postSolve called");
  }

  void preSolve(Contact contact, Manifold oldManifold) {
    //print ("postSolve called");
  }
}

part of world;

class Bullet extends GameObject {
  Weapon _weapon;

  Body get bulletBody => body;
       set bulletBody(bb) => body = bb;

   num get bulletDamage => _weapon.damage;
   num get damageRadius => _weapon.damageRadius;

   num get radius => _weapon.radius;

   Bullet(Vector2 pos, Vector2 direction, Weapon this._weapon, var world)
       : super(pos, direction, world) {
   }

   Body createBody(Vector2 pos, Vector2 linearVal, var world) {
     BodyDef bodyDef = new BodyDef();
     bodyDef.type = BodyType.DYNAMIC;
     bodyDef.position = pos;
     bodyDef.bullet = true;

     final bulletFixtureDef = new FixtureDef();
     final circleShape = new CircleShape();
     circleShape.radius = _weapon.radius;
     bulletFixtureDef.shape = circleShape;
     bulletFixtureDef.density = _weapon.bulletMass;

     body = world.createBody(bodyDef);
     body.linearVelocity = linearVal;
     body.createFixture(bulletFixtureDef);
     return body;
   }

   void destroy(var gameWorld) {
     // print("Destroying bullet");
     gameWorld.bulletToDelete.add(this);
   }

   // Serializatioin:
   Map toMap() {
     var map = super.toMap();
     map.addAll({
       'weapon': _weapon,
     });
     return map;
   }

   dynamic fromMap(Map map) {
     super.fromMap(map);
     var w = map['weapon'];
     _weapon = new Weapon()..fromJson(w);
   }
}


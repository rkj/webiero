part of renderer_2d;

class Animation {
  int _frame;
  int _maxFrames;
  int _xFrameSize;
  int _yFrameSize;
  CanvasImageSource _image;

  var position;
  num radius;

  Animation(String elemId, this.position, this._maxFrames, this._xFrameSize, this._yFrameSize, this.radius) {
    _frame = -1;
    _image = document.query('#${elemId}') as CanvasImageSource;
    assert(_image != null);
  }

  void step() {
    _frame += 1;
  }

  bool get isFinished => _frame >= _maxFrames;

  void draw(var context, Vector2 screenPos, num screenRadius) {
    //print("Drawing animation at $screenPos with $screenRadius");
    step();
    try {
      var xSize = screenRadius;
      var ySize = _xFrameSize / _yFrameSize * screenRadius;
      context.drawImageScaledFromSource(_image, _frame * _xFrameSize, 0,
          _xFrameSize, _yFrameSize,
          screenPos.x - xSize/2, screenPos.y - ySize/2,
          xSize, ySize);
    } catch (e) {
      print("Erorr drawing animation at $screenPos with $screenRadius");
    }
  }

  Animation.explosion(Bullet bullet, int no)
    : this("exp${no}", bullet.position, 36, 113, 105, bullet.damageRadius);
}



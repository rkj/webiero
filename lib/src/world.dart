part of world;

class GameWorld extends ConvertibleToJson {

  List<Player> players;

  World box2DWorld;
  num timestamp; // the timestamp at which this snapshot was taken.

  List<Polygon> terrain;
  List<Bullet> bullets;
  List<Bullet> bulletToDelete;
  var explosions;

  Body terrainBody;

  GameWorld() {
    init();
  }

  void init() {
    box2DWorld = new World(new Vector2(1.0, GRAVITY), true, new DefaultWorldPool());
    timestamp = new DateTime.now().millisecondsSinceEpoch;
    players = new List<Player>();
    bullets = new List<Bullet>();
    terrain = new List<Polygon>();
    explosions = new List<Bullet>();

    bulletToDelete = new List<Bullet>();
    BodyDef bodyDef = new BodyDef();
    bodyDef.type = BodyType.STATIC;
    bodyDef.position = new Vector2(0.0, 0.0);
    terrainBody = box2DWorld.createBody(bodyDef);

    // Set  Contact Listener
    GameContact contactListener = new GameContact(this);
    box2DWorld.contactListener = contactListener;
  }

  String toString() {
    return "$players";
  }

  Player createPlayer(String name, position, linearVelocity) {
    return new Player(position, linearVelocity, name, box2DWorld);
  }

  Player createPlayerFromJson(String json) {
    var player = new Player(new Vector2(0.0, 0.0), new Vector2(0.0, 0.0), '', box2DWorld);
    player.fromJson(json);
    return player;
  }

  Bullet createBullet(Player player, Weapon weapon, num strength) {
//    if (bullets.length > 50) {
//      bullets.clear();
//    }

    var direction = 1.0;
    if (player.turnedLeft) {
      direction = -1.0;
    }
    Vector2 dir = new Vector2(direction, 0.0);
    Matrix2 rotation = new Matrix2.rotation(-direction * player.shootingAngle);
    var position = player.position + (rotation * dir) * Player.RADIUS * 3.0;
    var speed = rotation * dir * weapon.shootingSpeed * strength + player.linearVelocity;
    var bullet = new Bullet(position, speed, player.weapon, box2DWorld);
    bullets.add(bullet);
    return bullet;
  }

  void createExplosion(Bullet bullet) {
    explosions.add(bullet);
  }

  List<Bullet> fetchExplosions() {
    var tmp = explosions;
    explosions = new List<Bullet>();
    return tmp;
  }

  GameWorld.dummy(var numPlayers) {
    init();
    for (var i=0; i<numPlayers; i++){
      Player player = new Player(new Vector2(10.0*i, i), new Vector2(10.0, -10.0), "Player $i", box2DWorld);
      players.add(player);
    }

    // Add four walls as terrain
    Polygon bottom_wall = new Polygon();
    bottom_wall.vertices = new List<Vector2>()
      ..add(new Vector2(100.0, 100.0))
      ..add(new Vector2(-100.0, 100.0))
      ..add(new Vector2(-100.0, 80.0))
      ..add(new Vector2(100.0, 80.0));

    Polygon top_wall = new Polygon();
    top_wall.vertices = new List<Vector2>()
        ..add(new Vector2(0.0, 0.0))
        ..add(new Vector2(100.0, 0.0));



    //add_terrain_to_world([bottom_wall]);
    add_random_terrain(400.0, 10.0, 300.0);
    //addPolygon(bottom_wall);
  }

  static const double GRAVITY = 10.0;
  static const double FRICTION = 10.0;

  void addPolygon(Polygon wall) {

      final terrainFixtureDef = new FixtureDef();
      final terrainShape = new PolygonShape();
      terrainShape.setFrom(wall.vertices, wall.vertices.length);
      terrainFixtureDef.shape = terrainShape;
      terrainFixtureDef.friction = FRICTION;

      terrainBody.createFixture(terrainFixtureDef);
      terrainBody.userData = wall;
      terrain.add(wall);
  }


  void add_random_terrain(double hlength, double step, double lowy) {
    var eps = 0.1;
    var rng = new Random(new DateTime.now().millisecondsSinceEpoch);
    var yval1 = 1.0 * rng.nextInt(10) + 75;
    for (var i = -hlength; i <= hlength; i += step) {
      var yval2 = 1.0 * rng.nextInt(20) + 75;
      var borderxr = 1.0 * i+step+eps;
      var borderxl = 1.0 * i-eps;
      Polygon terrainPolygon = new Polygon()
        ..vertices.add(new Vector2(borderxr, 1.0 * lowy))
        ..vertices.add(new Vector2(borderxl, 1.0 * lowy))
        ..vertices.add(new Vector2(borderxl, yval1))
        ..vertices.add(new Vector2(borderxr, yval2));
      addPolygon(terrainPolygon);
      yval1 = yval2;
    }
  }

  void addPlayer(Player player) {
    players.add(player);
  }

  Player getPlayerWithId(String playerId) {
    return players.firstWhere((player) => player.id == playerId);
  }

  void removePlayer(Player player) {
    players.remove(player);
  }

  void removePlayerWithId(String id) {
    players.removeWhere((player) => player.id == id);
  }


  /** The timestep and iteration numbers. */
  static const num TIME_STEP = 1/60;
  static const int VELOCITY_ITERATIONS = 10;
  static const int POSITION_ITERATIONS = 10;

  void update() {
    box2DWorld.step(TIME_STEP, VELOCITY_ITERATIONS, POSITION_ITERATIONS);
    for (final player in players) {
      player.position = player.playerBody.position;
      player.linearVelocity = player.playerBody.linearVelocity;
      /*_world.timestamp += TIME_STEP*1000;*/
    }
    for (final waste in bulletToDelete) {
      box2DWorld.destroyBody(waste.body);
      bullets.remove(waste);
    }
    bulletToDelete.clear();
  }

  // Serialization:
  Map toMap() {
    return {
      'players': players,
      'terrain': terrain,
    };
  }

  void fromMap(map) {
    map['players'].forEach((playerJson) {
      Player newPlayer = createPlayerFromJson(playerJson);
      addPlayer(newPlayer);
    });

    map['terrain'].forEach((polygonJson) {
      Polygon polygon = new Polygon();
      polygon.fromJson(polygonJson);
      addPolygon(polygon);
    });
  }
}

part of world;

abstract class GameObject extends ConvertibleToJson {
  String id;  // must be uniq in the entire world

  Vector2 get position => body.position;  // 1.0f == 1meter
       set position(pos) => body.setTransform(pos, 0.0);
  Vector2 get linearVelocity => body.linearVelocity;  // 1.0f == 1meter/second
       set linearVelocity(velocity) => body.linearVelocity = velocity;
  Body body;

  GameObject(Vector2 pos, Vector2 linearVel, var world) {
    body = createBody(pos, linearVel, world);
    body.userData = this;
    //print(this.runtimeType);
  }

  Body createBody(Vector2 pos, Vector2 linearVel, var world);

  // Serialization

  Map toMap() {
    var map = new Map();
    map['id'] = id;
    map['px'] = position.x;
    map['py'] = position.y;
    map['sx'] = linearVelocity.x;
    map['sy'] = linearVelocity.y;
    return map;
  }

  dynamic fromMap(Map map) {
    id = map['id'];
    position = new Vector2(1.0 * map['px'], 1.0 * map['py']);
    linearVelocity = new Vector2(1.0 * map['sx'], 1.0 * map['sy']);
    return this;
  }

  String toString() {
    return "id: $id, position: $position, speed: $linearVelocity";
  }
}


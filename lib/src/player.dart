part of world;

class Player extends GameObject {
  String name;
  var weapons = [new Weapon.bazooka(), new Weapon.uzi()];
  Weapon weapon;

  Body get playerBody => body;
       set playerBody(pb) => body = pb;
  Player(Vector2 pos, Vector2 linearVel, this.name, var world)
    : super(pos, linearVel, world) {
    weapon = weapons[0];
  }

  num shootingAngle = 0.0;
  var turnedLeft = true;
  var energy = 100.0;

  static const DIMENSION_X = 0.5;
  static const DIMENSION_Y = 2.0;
  static const RADIUS = 1.0;
  static const DENSITY = 0.5;

  Body createBody(Vector2 pos, Vector2 linearVel, var world) {
    BodyDef bodyDef = new BodyDef();
    bodyDef.type = BodyType.DYNAMIC;
    bodyDef.position = pos;

    final playerFixtureDef = new FixtureDef();
    final circleShape = new CircleShape();
    circleShape.radius = RADIUS;
    playerFixtureDef.shape = circleShape;
    playerFixtureDef.density = DENSITY;
    playerFixtureDef.friction = GameWorld.FRICTION;

    body = world.createBody(bodyDef);
    body.linearVelocity = linearVel;
    body.createFixture(playerFixtureDef);
    return body;
  }

  void hitByBullet(var bullet) {
    energy = energy - bullet.bulletDamage;
    print ("energy down to $energy");
  }

  // Serializatioin:

  Map toMap() {
    var map = super.toMap();
    map.addAll({
      'name': name,
      'shootingAngle': shootingAngle,
      'turnedLeft': turnedLeft,
      'energy': energy,
    });
    return map;
  }

  dynamic fromMap(Map map) {
    super.fromMap(map);
    name = map['name'];
    shootingAngle = map['shootingAngle'];
    turnedLeft = map['turnedLeft'];
    energy = map['energy'];
  }

  var shootingStrength = 0;
  void shootStart(GameWorld world, var socket, num time, num dt) {
    if (!weapon.isLoaded(time)) {
      return;
    }
    // we want to have full strength in half the reload time
    shootingStrength += dt / (weapon.reloadTime / 2);
    if (shootingStrength >= 1.0) {
      shootFinish(world, socket, time);
    }
  }

  void shootFinish(GameWorld world, var socket, num time) {
    if (shootingStrength <= 0.1) {
      shootingStrength = 0;
      return;
    }
    weapon.shoot(time);
    Bullet bullet = world.createBullet(this, weapon, shootingStrength);
    new Message.Shoot(bullet).send(socket);
    shootingStrength = 0;
  }

  void jump(num time) {
    if (linearVelocity.y.abs() <= 0.1) {
      linearVelocity.y -= JUMP_SPEED;
    }
  }

  void selectWeapon(int weaponNum) {
    if (weaponNum < 0 || weaponNum > weapons.length) {
      return;
    }
    weapon = weapons[weaponNum];
  }

  static const num MAX_SPEED = 20.0;
  static const num ACCELERATION = 50;
  static const num JUMP_SPEED = 8;

  void moveLeft(num dt) {
    turnedLeft = true;
    if (linearVelocity.x > -MAX_SPEED) {
      linearVelocity.x -= ACCELERATION * dt;
    }
  }

  void moveRight(num dt) {
    turnedLeft = false;
    if (linearVelocity.x < MAX_SPEED) {
      linearVelocity.x += ACCELERATION*dt;
    }
  }

  String toString() {
    return "id: $id, name: $name, position: ($position), speed: ($linearVelocity), shootingAngle: $shootingAngle, turnedLeft: $turnedLeft";
  }
}


library renderer_2d;

import 'dart:html';
import 'dart:math';
import 'package:box2d/box2d_browser.dart';
import 'package:game_loop/game_loop_html.dart';
import 'package:webiero/world.dart';

part 'src/animation.dart';

class Renderer2D {
  var _canvas;
  GameWorld _world;
  Player _player;
  List<Animation> animations;

  CanvasRenderingContext2D _context;
  Vector2 _viewport;
  Vector2 _view;

  // 1.0 - one meter = one pixel
  // 10.0 - one meter = ten pixels
  double zoomLevel = 10.0;

  void incZoomLevel() {
    if (zoomLevel > 1) {
      zoomLevel -= 1;
    }
  }

  void decZoomLevel() {
    if (zoomLevel < 20) {
      zoomLevel += 1;
    }
  }

  Renderer2D(canvas, world, player) {
    _world = world;
    _player = player;
    _context = canvas.getContext("2d") as CanvasRenderingContext2D;
    _canvas = canvas;
    animations = new List<Animation>();

    canvas.width = canvas.clientWidth;
    canvas.height = canvas.clientHeight;

    _viewport = new Vector2(1.0 * canvas.width, 1.0 * canvas.height);

    setupDebugDraw();
  }

  set currentPlayer(player) => _player = player;

  void createExplosions(List<Bullet> explosions) {
    for (Bullet bullet in explosions) {
      var anim = new Animation.explosion(bullet, 4);
      animations.add(anim);
    }
  }

  void resize(GameLoopHtml gameLoop) {
    CanvasElement canvas = gameLoop.element;
    canvas.width = canvas.clientWidth;
    canvas.height = canvas.clientHeight;
    _viewport = new Vector2(1.0 * canvas.width, 1.0 * canvas.height);
//    print("Resizing to $_viewport");
  }

  void render(GameLoopHtml gameLoop) {
    var viewSize = _viewport / zoomLevel;
    _view = _player.position - (viewSize/2.0);

    drawBackground();
    drawClouds();
    drawTerrain(_world.terrain);
    drawBullets(_world.bullets);
    drawPlayers(_world.players);
    drawAnimations(animations);
  }

  void drawBackground() {
    // Create a gradient object from the canvas context.
    var gradient = _context.createLinearGradient(0, 0, 0, _viewport.y);

    // Add the colors with fixed stops.
    gradient.addColorStop(0, '#CCE5FF');
    gradient.addColorStop(1, '#3399FF');

    _context.fillStyle = gradient;
    _context.fillRect(0, 0, _viewport.x, _viewport.y);
  }

  void drawClouds() {
    CanvasImageSource cloud1 = document.querySelector('#cloud1') as CanvasImageSource;
    CanvasImageSource cloud2 = document.querySelector('#cloud2') as CanvasImageSource;
    CanvasImageSource cloud3 = document.querySelector('#cloud3') as CanvasImageSource;

    _context.drawImageScaled(cloud1, 50, 100, 100, 100);
    _context.drawImageScaled(cloud2, 400, 50, 100, 100);
    _context.drawImageScaled(cloud3, 700, 150, 100, 100);
  }

  void drawTerrain(terrains) {
    for (Polygon polygon in _world.terrain) {
      Vector2 firstVertexScreenPos;
      var started = false;
      _context.beginPath();
      for (Vector2 vertex in polygon) {
        Vector2 screenPos = (vertex - _view) * zoomLevel;
        if (!started) {
          _context.moveTo(screenPos.x, screenPos.y);
          firstVertexScreenPos = screenPos;
          started = true;
          continue;
        }
        _context.lineTo(screenPos.x, screenPos.y);
      }
      _context.closePath();

      // translate & scale
      num scaleFactor = 0.05 * zoomLevel;
      _context.translate(firstVertexScreenPos.x, firstVertexScreenPos.y);
      _context.scale(scaleFactor, scaleFactor);

      CanvasImageSource terrain1 = document.querySelector('#terrain1') as CanvasImageSource;
      CanvasPattern pattern = _context.createPattern(terrain1, 'repeat');
      _context.fillStyle = pattern;
      _context.fill();

      // undo translate & scale
      _context.scale(1/scaleFactor, 1/scaleFactor);
      _context.translate(-firstVertexScreenPos.x, -firstVertexScreenPos.y);

      _context.fillStyle = '#000';
    }
  }

  void drawPlayers(players) {
    _context.fillStyle = "#000";
    _context.fillText("${_view.x.toStringAsFixed(2)}, ${_view.y.toStringAsFixed(2)}", 0, 10);
    var bodySize = new Vector2(Player.DIMENSION_X, Player.DIMENSION_Y) * zoomLevel;
    var headSize = new Vector2(Player.DIMENSION_Y, Player.DIMENSION_X) / 2.0 * zoomLevel;
    for (var player in players) {
      var pos = player.position;
      var screenPos = (pos - _view) * zoomLevel;
      _context.translate(screenPos.x, screenPos.y);
      if (player.energy <= 0) {
        _context.rotate(PI/2);
      }

      var energyLength = 10 * zoomLevel;
      TextMetrics metrics = _context.measureText(player.name);
      if (metrics.width <= 2*energyLength) {
         _context.fillText(player.name, -metrics.width/2, -2 * zoomLevel - bodySize.y);
      }

      if (player.energy > 0) {
        _context.fillStyle = "#0F0";
        _context.fillRect(-energyLength / 2, -zoomLevel - bodySize.y,
            energyLength * player.energy / 100, zoomLevel);
      }

      if (player.shootingStrength > 0) {
        const int border = 1;
        _context.fillStyle = "#FFF";
        var ypos = -2 * zoomLevel - bodySize.y - border;
        _context.fillRect(-energyLength / 2, ypos,
            energyLength, zoomLevel);
        _context.fillStyle = "#00F";
        _context.fillRect(-energyLength / 2 + border, ypos + border,
            (energyLength - 2 * border) * player.shootingStrength,
            zoomLevel - 2 * border);
      }

      int direction = 1;
      if (player.turnedLeft) {
       direction = -1;
      }

      _context.fillStyle = "#FF0";
      var angle = player.shootingAngle;
      _context.translate(0, -bodySize.y / 4);
      _context.rotate(-angle * direction);
      _context.fillRect(0, 0, direction * headSize.x * 3, headSize.y*2);
      _context.rotate(angle * direction);
      _context.translate(0, bodySize.y / 4);

      _context.fillStyle = "#A00";
      _context.fillRect(-bodySize.x/2, -bodySize.y/2, bodySize.x, bodySize.y);

      _context.fillStyle = "rgb(255,0,0)";
      _context.beginPath();
      _context.arc(0, -bodySize.y/2, bodySize.x, 0, 2*PI);
      _context.closePath();
      _context.fill();

      if (player.energy <= 0) {
        _context.rotate(-PI/2);
      }
      _context.translate(-screenPos.x, -screenPos.y);
    }
  }

  void drawBullets(bullets) {
    _context.fillStyle = "#000";
    for(var bullet in bullets) {
      var pos = bullet.position;
      var screenPos = (pos - _view) * zoomLevel;
      _context.beginPath();
      _context.arc(screenPos.x, screenPos.y, bullet.radius * zoomLevel, 0, 2*PI);
      _context.closePath();
      _context.fill();
    }
  }

  void drawAnimations(var animations) {
    var toRemove = [];
    for (var animation in animations) {
      drawAnimation(animation);
      if (animation.isFinished) {
        toRemove.add(animation);
      }
    }
    toRemove.forEach((anim) => animations.remove(anim));
  }

  void drawAnimation(var animation) {
    Vector2 screenPos = (animation.position - _view) * zoomLevel;
    var radius = animation.radius * zoomLevel;
    animation.draw(_context, screenPos, radius);
  }

  void drawFinish() {
    _context.save();
    _context.fillStyle = "rgba(0, 0, 0, 0.5)";
    _context.fillRect(0, 0, _viewport.x, _viewport.y);
    _context.fillStyle = "rgba(1, 0, 0)";
    _context.font = "40pt Arial";
    var txt = "Game finished (for you)";
    var metrics = _context.measureText(txt);
    _context.fillText(txt,
        _viewport.x / 2 - metrics.width / 2,
        _viewport.y / 2);
    _context.restore();
  }

  void setupDebugDraw() {
    final extents = _viewport;
    var viewport = new CanvasViewportTransform(extents, extents);
    viewport.scale = zoomLevel;

    // Create our canvas drawing tool to give to the world.
    var debugDraw = new CanvasDraw(viewport, _context);

    // Have the world draw itself for debugging purposes.
    _world.box2DWorld.debugDraw = debugDraw;
  }
}

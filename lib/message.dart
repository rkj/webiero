library message;

import 'dart:convert';
import 'package:webiero/world.dart';

// All suported message types:

const int kConnectPlayer = 1;
const int kDisconnectPlayer = 2;

const int kInitWorldState = 3;
const int kInitPlayerState = 4;
const int kInitializationFinished = 5;

const int kAddPlayer = 6;
const int kRemovePlayer = 7;

const int kShoot = 8;

const int kUpdatePlayerState = 9;
const int kUpdateGameState = 10;

class Message {
  Map<String, Object> map;

  Message(this.map);

  Message.ConnectPlayer(playerName) {
    map = {
      'type': kConnectPlayer,
      'payload': playerName,
    };
  }

  Message.DisconnectPlayer() {
    map = {
      'type': kDisconnectPlayer,
    };
  }

  Message.InitWorldState(world) {
    map = {
      'type': kInitWorldState,
      'payload': world,
    };
  }

  Message.InitPlayerState(player) {
    map = {
      'type': kInitPlayerState,
      'payload': player,
    };
  }

  Message.InitializationFinished() {
    map = {
      'type': kInitializationFinished,
    };
  }

  Message.AddPlayer(player) {
    map = {
      'type': kAddPlayer,
       'payload': player,
    };
  }

  Message.RemovePlayer(id) {
    map = {
      'type': kRemovePlayer,
       'payload': id,
    };
  }

  Message.UpdatePlayerState(player) {
    map = {
      'type': kUpdatePlayerState,
      'payload': player,
    };
  }

  Message.Shoot(bullet) {
    map = {
      'type': kShoot,
      'payload': bullet,
    };
  }

  Message.UpdateGameState(List<GameObject> list) {
    map = {
      'type': kUpdateGameState,
      'payload': list,
    };
  }


  static Message parse(data) {
    return new Message(JSON.decode(data));
  }

  void send(var socket) {
    var data = JSON.encode(this.map);
    try {
      socket.add(data);
    } catch(_) {
      socket.send(data);
    }
  }

  int get type => map['type'];
  String get payload => map['payload'];
}

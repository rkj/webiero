library world;

import 'dart:convert';
import 'dart:math';
import 'package:box2d/box2d.dart';
import 'package:webiero/message.dart';

part 'src/bullet.dart';
part 'src/contact.dart';
part 'src/game_object.dart';
part 'src/player.dart';
part 'src/polygon.dart';
part 'src/weapon.dart';
part 'src/world.dart';

abstract class ConvertibleToJson {
  Map toMap();
  dynamic fromMap(map);

  String toJson() {
    return JSON.encode(this.toMap());
  }

  dynamic fromJson(String json) {
    return this.fromMap(JSON.decode(json));
  }
}

